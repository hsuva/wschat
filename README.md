## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Download Git (https://git-scm.com/downloads)

If Git and Composer are installed, run

```bash
git clone https://hsuva@bitbucket.org/hsuva/wschat.git
cd wschat
composer install
php cmd.php
```
