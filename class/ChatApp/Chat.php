<?php
namespace ChatApp;


use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {
    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        $this->sendActiveUserCount('open');
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $data = json_decode($msg);
        
        if($data->text == '/quit'){            
            $this->clients->detach($from);
            echo "Connection {$from->resourceId} has disconnected\n";
            return;
        }

        foreach ($this->clients as $client) {
            if ($from !== $client) {               
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {    
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
        $this->sendActiveUserCount('close');
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    private function sendActiveUserCount($event){
        $userCount = count($this->clients);
        foreach ($this->clients as $client) {
            $client->send(json_encode([
                'event' => $event,
                'data' => $userCount
            ]));
        }
    }
}